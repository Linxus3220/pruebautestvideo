#Autor: Miguel Alvarez
  @stories
  Feature: Registro en la pagina de utest
    @Scenario
    Scenario: Juan se quiere registrar en la pagina de Utest
      Given El desea registrarse en la pagina de utest
      When Diligencia todos los datos para el registro
        |strNombre|strApellido|strEmail            |strMes|strDia|strAno|strCiudad|strCdigoPostal|strPais |strCompuatdor |strVersion |strLenguaje |strMovil |strModelo |strOS   |
        |Nombre   |Apellido   |linxus3220@gmail.com|May   |15    |1990  |Sincelejo|700001        |Colombia|Windows       |2000       |Arabic      |Apple    |iPhone X  |iOS 14.4|
      Then Termina el registro al ver el boton de Complete Setup
        |strTextoBotonFinal|
        |Complete Setup    |