package co.com.utestFinal.Runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/features/utestFinal.feature",
        tags = "@stories",
        glue = "co.com.utestFinal.Stepdefinitions",
        snippets = SnippetType.CAMELCASE )

public class RunnersTags {
}