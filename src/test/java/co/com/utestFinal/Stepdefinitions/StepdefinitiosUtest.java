package co.com.utestFinal.Stepdefinitions;

import co.com.utestFinal.model.DatosUtest;
import co.com.utestFinal.questions.Respuesta;
import co.com.utestFinal.tasks.AbrirPagina;
import co.com.utestFinal.tasks.LlenarDireccion;
import co.com.utestFinal.tasks.LlenarDispositivos;
import co.com.utestFinal.tasks.LlenarInformacionGeneral;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class StepdefinitiosUtest {

    @Before
    public void setOnStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^El desea registrarse en la pagina de utest$")
    public void elDeseaRegistrarseEnLaPaginaDeUtest(){
        theActorCalled("Juan").attemptsTo(AbrirPagina.enLaPagina());
    }

    @When("^Diligencia todos los datos para el registro$")
    public void diligenciaTodosLosDatosParaElRegistro(List<DatosUtest> datos){
        theActorInTheSpotlight().attemptsTo(LlenarInformacionGeneral.enLaPagina(datos),
                LlenarDireccion.enLaPagina(datos),
                LlenarDispositivos.enLaPagina(datos),
                LlenarSeguridad.enLaPagina(datos));
    }

    @Then("^Termina el registro al ver el boton de Complete Setup$")
    public void terminaElRegistroAlVerElBotonDeCompleteSetup(List<DatosUtest> datos){
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(Respuesta.es(datos)));
    }
}
