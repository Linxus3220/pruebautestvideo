package co.com.utestFinal.Stepdefinitions;

import co.com.utestFinal.model.DatosUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static co.com.utestFinal.userinterfaces.PaginaSeguridad.*;

public class LlenarSeguridad implements Task {

    private List<DatosUtest> datos;

    public LlenarSeguridad(List<DatosUtest> datos) {
        this.datos = datos;
    }

    public static LlenarSeguridad enLaPagina(List<DatosUtest> datos) {
        return Tasks.instrumented(LlenarSeguridad.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(datos.get(0).getStrContrasena()).into(CAMPO_CONTRASENA),
                Enter.theValue(datos.get(0).getStrContrasenaConfirmar()).into(CAMPO_CONTRASENA_CONFIRMAR),
                Click.on(CHECK_1),
                Click.on(CHECK_2),
                Click.on(CHECK_3)
        );

    }
}
