package co.com.utestFinal.questions;

import co.com.utestFinal.model.DatosUtest;
import co.com.utestFinal.userinterfaces.PaginaSeguridad;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class Respuesta implements Question<Boolean> {
    private List<DatosUtest> datos;

    public Respuesta(List<DatosUtest> datos) {
        this.datos = datos;
    }

    public static Respuesta es(List<DatosUtest> datos) {
        return new Respuesta(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        String texto_boton = Text.of(PaginaSeguridad.TEXTO_BOTON_FIANL).viewedBy(actor).asString();
        return datos.get(0).getStrTextoBotonFinal().equals(texto_boton);
    }
}
