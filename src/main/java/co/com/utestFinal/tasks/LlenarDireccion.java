package co.com.utestFinal.tasks;

import co.com.utestFinal.model.DatosUtest;
import co.com.utestFinal.userinterfaces.PaginaDireccion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import java.util.List;

import static co.com.utestFinal.userinterfaces.PaginaDireccion.*;

public class LlenarDireccion implements Task {
    private List<DatosUtest> datos;

    public LlenarDireccion(List<DatosUtest> datos) {
        this.datos = datos;
    }

    public static LlenarDireccion enLaPagina(List<DatosUtest> datos) {
        return Tasks.instrumented(LlenarDireccion.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(datos.get(0).getStrCiudad()).into(CAMPO_CIUDAD),
                Enter.theValue(datos.get(0).getStrCdigoPostal()).into(CAMPO_CODIGO_POSTAL),
                Click.on(DIV_PAIS),
                Enter.theValue(datos.get(0).getStrPais()).into(CAMPO_PAIS),
                Click.on(BOTON_NEXT)
        );

    }
}
