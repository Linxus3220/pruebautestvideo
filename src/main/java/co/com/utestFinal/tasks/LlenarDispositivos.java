package co.com.utestFinal.tasks;

import co.com.utestFinal.model.DatosUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import java.util.List;

import static co.com.utestFinal.userinterfaces.PaginaDispositivos.*;

public class LlenarDispositivos implements Task {
    private List<DatosUtest> datos;

    public LlenarDispositivos(List<DatosUtest> datos) {
        this.datos = datos;
    }

    public static LlenarDispositivos enLaPagina(List<DatosUtest> datos) {
        return Tasks.instrumented(LlenarDispositivos.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(DIV_PC),
                Enter.theValue(datos.get(0).getStrCompuatdor()).into(CAMPO_PC).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(DIV_VERSION),
                Enter.theValue(datos.get(0).getStrVersion()).into(CAMPO_VERSION).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(DIV_LENGUAJE),
                Enter.theValue(datos.get(0).getStrLenguaje()).into(CAMPO_LENGUAJE).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(DIV_MOVIL),
                Enter.theValue(datos.get(0).getStrMovil()).into(CAMPO_MOVIL).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(DIV_MODELO),
                Enter.theValue(datos.get(0).getStrModelo()).into(CAMPO_MODELO).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(DIV_OS),
                Enter.theValue(datos.get(0).getStrOS()).into(CAMPO_OS).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(BOTON_NEXT)
        );

    }
}
